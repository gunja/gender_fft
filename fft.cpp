/*
 * fft.cpp file
 * copy of exisiting file
*/

#include <math.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>

#define NR_END 1
#define FREE_ARG char*
#ifndef M_PI
#define M_PI 3.14159265358979323846264338327
#endif

static double sqrarg ;
#define SQR(a) ( ( sqrarg =(a)) == 0.0?0.0:sqrarg*sqrarg  )
#define WINDOW( j , a , b ) (1.0- fabs ( ( ( ( j )-1)-( a ) ) * ( b ) ) ) /* B a r t l e t t */

void nrerror ( char error_text [ ] )
/* Nume r i cal Re c i p e s s t a n d a r d e r r o r h a n d l e r */
{
    fprintf(stderr, "Numerical Recipes run-time error ... \n" ) ;
    fprintf(stderr, "%s\n", error_text ) ;
    fprintf(stderr, "... now exiting to system ... \n" ) ;
    exit ( 1 ) ;
}
double * vector ( long nl , long nh )
/* a l l o c a t e a d oub l e v e c t o r wi t h s u b s c r i p t range v [ n l . . nh ] */
{
    double *v ;
    v =( double *) malloc ( ( size_t )( ( nh-nl + 1+NR_END )* sizeof( double ) ) ) ;
    if( !v ) nrerror ( "allocation failure in vector ( )" ) ;
    return v-nl +NR_END;
}
void free_vector ( double *v , long nl , long nh )
/* f r e e a d ou bl e v e c t o r a l l o c a t e d wi t h v e c t o r ( ) */
{
    free ( ( FREE_ARG) ( v+nl-NR_END ) ) ;
}
#define SWAP( a , b ) tempr =( a ) ; ( a ) = ( b ) ; ( b )= tempr

void four1 ( double data [ ] , unsigned long nn , int isign )
{
    unsigned long n ,mmax ,m, j , istep , i ;
    double wtemp , wr , wpr , wpi , wi , theta ;
    double tempr , tempi ;
    n=nn << 1 ;
    j =1;
    for ( i =1; i<n ; i +=2) {
        if ( j > i ) {
            SWAP( data [ j ] , data [ i ] ) ;
            SWAP( data [ j +1] , data [ i + 1 ] ) ;
        }
        m=n >> 1 ;
        while (m >= 2 && j > m) {
            j -= m;
            m >>= 1 ;
        }
        j += m;
    }
    mmax=2;
    while ( n > mmax) {
        istep =mmax << 1 ;
        theta = isign * ( 6.28318530717959 /mmax ) ;
        wtemp= sin(0.5 * theta ) ;
        wpr = -2.0*wtemp*wtemp ;
        wpi= sin(theta) ;
        wr = 1.0 ;
        wi = 0.0 ;
        for (m=1;m<mmax ;m+=2) {
            for ( i =m; i<=n ; i += istep ) {
                j = i +mmax ;
                tempr=wr* data[j]-wi* data [ j + 1 ] ;
                tempi=wr* data[j+1]+wi* data[j] ;
                data[j]= data[i]-tempr ;
                data[j +1]= data[i+1]-tempi ;
                data[i] += tempr ;
                data[i+1]+=tempi ;
            }
            wr =(wtemp=wr )*wpr-wi*wpi+wr ;
            wi=wi*wpr+wtemp*wpi+wi ;
        }
        mmax= istep ;
    }
}

void realft( double data[] , unsigned long n, int isign )
{
    //void f o u r 1 ( double d a t a [ ] , unsigned long nn , i n t i s i g n ) ;
    unsigned long i , i1 , i2 , i3 , i4 , np3 ;
    double c1 =0.5 , c2 , h1r , h1i , h2r , h2i ;
    double wr , wi , wpr , wpi , wtemp , theta ;
    theta =3.141592653589793/ ( double ) ( n>>1);
    if ( isign == 1){
        c2 = -0.5;
        four1 ( data , n>>1 ,1);
    } else {
        c2 = 0.5 ;
        theta = -theta ;
    }
    wtemp= sin (0.5* theta ) ;
    wpr = -2.0*wtemp*wtemp ;
    wpi= sin( theta ) ;
    wr =1.0+wpr ;
    wi=wpi ;
    np3=n + 3;
    for( i =2; i <=(n>>2); i ++) {
        i4 =1+( i3 =np3-( i2 =1+( i1 = i +i -1 ) ) ) ;
        h1r=c1 *( data[i1]+ data[i3] ) ;
        h1i =c1 *(data[i2]-data[i4] ) ;
        h2r = -c2*(data[i2]+ data[i4] ) ;
        h2i =c2 *(data[i1]-data[i3] ) ;
        data[i1]= h1r+wr* h2r-wi* h2i ;
        data[i2]= h1i +wr* h2i +wi*h2r ;
        data[i3]= h1r-wr*h2r+wi*h2i ;
        data[i4] = -h1i +wr*h2i +wi*h2r ;
        wr =(wtemp=wr )*wpr-wi*wpi+wr ;
        wi=wi*wpr+wtemp*wpi+wi ;
    }
    if(isign == 1) {
        data[1] = ( h1r= data[1] ) + data[2] ;
        data[2] = h1r-data[2] ;
    } else {
        data[1]= c1*(( h1r= data[1] ) + data[2] ) ;
        data[2]= c1 *( h1r-data[2] ) ;
        four1( data , n>>1,-1);
    }
}
