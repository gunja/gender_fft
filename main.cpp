#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include "fft.h"
#include <QFile>

#ifndef M_PI
#define M_PI 3.14159265358979323846264338327
#endif
#define OUTPUTRATE 48000
#define SPECTRUMSIZE 8192
#define SPECTRUMRANGE ((float)OUTPUTRATE/ 2.0f)
#define BINSIZE (SPECTRUMRANGE /(float)SPECTRUMSIZE )

using namespace std;
struct  wavfile
{
    char id[ 4 ] ; // s h o u l d a lways  c o n t a i n ”RIFF”
    int32_t spectrumlength ; // s p e c t r um f i l e l e n g t h minus 8
    char wavefmt[ 8 ] ; // s h o u l d be ”WAVEfmt ”
    int32_t format ; // 16 f o r PCM f o rma t
    int16_t pcm; // 1 f o r PCM f o rma t
    int16_t channels ; // c h a n n e l s
    int32_t frequency ; // s amp l i ng f r e q u e n c y
    int32_t bytes_per_second ;
    int16_t bytes_by_capture ;
    int16_t bits_per_sample;
    char data[4];
    int32_t bytes_in_data;
};

int main ( int argc , char * argv[] )
{
    char filename [ 120 ] ;
    if( argc < 2) {
        fprintf( stderr, "error on parameters count\n");
        exit(1);
    }
    sprintf(filename, "%s" , argv [ 1 ] ) ;

    // should be transfered to QFile
    QFile ff(filename);
    if( ff.exists()) ff.open( QIODevice::ReadOnly ); // will be opened in binary mode
    if( ! ff.isOpen() ) {
        fprintf( stderr, "failed to open file %s", argv[1]);
        fprintf(stderr, "Aborting\n");
        exit(1);
    }
    struct wavfile header ;

// read heade r
    if( ff.read(reinterpret_cast<char *>(&header), sizeof ( header ) ) != sizeof ( header ) )
    {
       fprintf ( stderr , "Can ’ t r e a d i n p u t f i l e h e a d e r %s \n" ,
                    filename ) ;
        exit ( 1 ) ;
    }

// read data
    int16_t value ;
    double data [SPECTRUMSIZE], temp[SPECTRUMSIZE], spectrum[SPECTRUMSIZE];
    double buffer[SPECTRUMSIZE / 2 ] , weight , sum;
    int i, count, length, bin = 0, offset, counter=1, original_bin = 0 ;
    int divideby2 , divideby3, start = 0, male_count = 0, female_count = 0, buffer_count = 0 ;
    bool running = true, flag , found = false ;
    float percent_thresh = 0.1f, fmax = 275, max, max_thresh = 0.001, cap_thresh = 0.015 ;
    float new_sum = 0 , original_max , eps = 1e-7, pitch , pitch_sum = 0 ;
    // g a u s s i a n   sigma = 5:
    float gaussian[11] = { 0.0663 , 0.0794 , 0.0914 ,
            0.1010, 0.1072 , 0.1094 ,
            0.1072, 0.1010 , 0.0914 ,
            0.0794 , 0.0663 } ;

    bool first = true ;
    while ( running )
    {
        count = 0 ;
        if ( first )
        {
            while ( running && count < (SPECTRUMSIZE - (SPECTRUMSIZE / 2) ) )
            {
                //running = fread (&value , sizeof ( value ) , 1 , wav ) ;
                running = (ff.read( reinterpret_cast<char *>(&value), sizeof ( value ) ) > 0);
                data [ count ] = ( double ) ( value / ( pow ( 2. , 15 ) ) ) ;
                ++count ;
            }
            first = false;
        } else {
            while ( running && count < (SPECTRUMSIZE - (SPECTRUMSIZE / 2) ) )
            {
                data [ count ] = buffer [ count ] ;
                ++count ;
            }
        }
        buffer_count = 0 ;
        while ( count < SPECTRUMSIZE)
        {
            //running = fread (&value , sizeof ( value ) , 1 , wav ) ;
            running = (ff.read( reinterpret_cast<char *>(&value), sizeof ( value ) ) > 0);
            data [ count ] = ( double ) ( value / ( pow ( 2. , 15 ) ) ) ;
            buffer [ buffer_count ] = data [ count ] ;
            ++count ;
            ++buffer_count ;
        }
        // Apply hamming window :
        for ( i = 0 ; i < SPECTRUMSIZE; i ++)
        {
            weight = 0.5 - 0.46 * cos(2 * M_PI * ( double )i/(double) ( SPECTRUMSIZE - 1 ) ) ;
            data[i] = weight * data [ i ] ;
        }
        // Take t h e f f t
        realft ( data , SPECTRUMSIZE, 1 ) ;
        // Combine   r e a l and ima g i n a r y f r e q u e n c y p a r t s
        for ( i = 0 ; i < SPECTRUMSIZE / 2 ; ++ i ) {
            temp [ i ] = hypot ( data [ i ] , data [ i + SPECTRUMSIZE / 2 ] ) ;
        }
        // Smooth
        for ( i = 0 ; i < SPECTRUMSIZE / 2 ; ++ i ){
            sum = 0 ;
            for ( int j = SPECTRUMSIZE / 2 ; j < 11 + SPECTRUMSIZE / 2 ; ++ j ) {
                sum += temp [ ( i + j - 5) % (SPECTRUMSIZE / 2 ) ] * gaussian [ j - SPECTRUMSIZE / 2 ] ;
            }
            spectrum [ i ] = sum / 11 ;
        }

        // Now i d e n t i f y gende r :
        sum = 0 ;
        counter = 1 ;
        max = 0 ;
        while ( counter < ( fmax / BINSIZE ) )  {
            sum += spectrum [ counter ] ;
            ++counter ;
        }
        counter = 1 ;

        // Perform Capping
        while ( counter < ( fmax / BINSIZE ) )
        {
            if ( spectrum [ counter ] > cap_thresh * sum)
            {
                new_sum += cap_thresh * sum;
            } else {
                new_sum += spectrum[counter];
            }
            ++counter;
        }
        sum = new_sum ;
        counter = ( int ) ( 75 / BINSIZE ) ;
        while ( counter < ( fmax / BINSIZE ) )
        {
            if ( spectrum [ counter ] > max )
            {
                max = spectrum [ counter ] ;
                bin = counter ;
            }
            ++counter ;
        }

        // che c k lowe r harmoni c s :
        offset = ( int ) ( 15 / BINSIZE ) ;
        original_max = max ;
        original_bin = bin ;
        if ( original_bin > ( int ) ( 150 / BINSIZE ) )
        {
            divideby2 = ( int ) ( bin / 2 ) ;
            counter = divideby2 - ( int ) ( 30 / BINSIZE ) ;
            found = false ;
            while ( ( counter < divideby2 + ( int ) ( 30 / BINSIZE ) ) && ! found )
            {
                if (((  1 - spectrum [ counter - offset ] / spectrum [ counter ] ) +
                        (1 - spectrum[counter + offset ] / spectrum [ counter ] ) ) / 2 > percent_thresh &&
                            spectrum [ counter ] > ( max_thresh * sum ) )
                {
                    max = spectrum [ counter ] ;
                    flag = true ;
                    for ( i = counter ; i > counter - (int)( 15 / BINSIZE ) ; --i )
                    {
                        if(spectrum[i] > max + eps )
                        {  flag = false ; }
                    }
                    for( i = counter ; i < counter + (int)(15/BINSIZE ); ++i )
                    {
                        if(spectrum[i] > max + eps )
                        { flag = false ; }
                    }
                    if(flag ){
                        bin = counter ;
                        found = true ;
                    }
                }
                ++counter ;
            }
        }
        if(original_bin > (int)( 220 / BINSIZE ) )
        {
            divideby3 = (int) (bin/3 ) ;
            counter = divideby3 - ( int ) ( 30 / BINSIZE ) ;
            found = false ;
            while( (counter < divideby3 + (int) ( 30 / BINSIZE ) ) && ! found )
            {
                if((( 1 - spectrum[counter - offset]/ spectrum[counter] ) +
                    (1 - spectrum[counter + offset]/spectrum[counter] ) ) / 2 > percent_thresh &&
                        spectrum[counter] > ( max_thresh * sum ) )
                {
                    max = spectrum[counter];
                    flag = true ;
                    for( i = counter ; i > counter - (int)(15/BINSIZE ) ; --i )
                    {
                        if ( spectrum[i] > max + eps )
                        { flag = false ; }
                    }
                    for( i = counter ; i < counter+(int)(15/BINSIZE ) ; ++i )
                    {
                        if (spectrum[i] > max + eps ) {
                            flag = false ; }
                    }
                    if(flag) {
                        bin = counter ;
                        found = true ;
                    }
                }
                    ++counter;
            }
        }
        pitch = bin * (float ) BINSIZE ;
        if(pitch < 300 && pitch > 165)
        {
            ++female_count ;
            pitch_sum += pitch ;
        } else if(pitch < 150 && pitch > 70){
            ++male_count ;
            pitch_sum += pitch ;
        } else {}
    }
    // c o u t << ” t a k i n g ” << dwEnd 􀀀 dwS t a r t << ” mi l l i s e c o n d s ”;
    ofstream myfile ;
        myfile.open("output.txt", fstream::in | fstream::out | fstream::app ) ;
        if( ( female_count - male_count ) > 0)
        {
            myfile << "Female\n";
        // c o u t << ” C l a s s i f i e d Gender = Female” << e n d l ;
        } else if ( ( male_count - female_count ) > 0)
        {
            myfile << "Male\n" ;
        // c o u t << ” C l a s s i f i e d Gender = Male” << e n d l ;
        } else {
            if ( ( pitch_sum/ ( male_count + female_count ) ) > 157) {
                myfile << "Female\n" ;
            // c o u t << ” C l a s s i f i e d Gender = Female” << e n d l ;
            } else {
                myfile << "Male\n" ;
            // c o u t << ” C l a s s i f i e d Gender = Male” << e n d l ;
            }
        }
        myfile.close() ;
    return 0;
}
