#-------------------------------------------------
#
# Project created by QtCreator 2014-12-15T15:03:53
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = gender_det
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += \
    ../fft.cpp \
    ../main.cpp \
    c_main.cpp \
    spectrum.cpp

HEADERS += \
    ../fft.h \
    spectrum.h

FORMS +=
