#ifndef SPECTRUM_H
#define SPECTRUM_H

class Spectrum
{
    const static int SPECTRUMSIZE = 8192;
    const static int OUTPUTRATE= 48000;
    const float gaussian[11] = { 0.0663 , 0.0794 , 0.0914 ,
                           0.1010, 0.1072 , 0.1094 ,
                           0.1072, 0.1010 , 0.0914 ,
                           0.0794 , 0.0663 } ;
    double *data,  *temp, *spectrum;

public:
    Spectrum();
    void performCapping();
    bool readInput();
    void Smooth();
};

#endif // SPECTRUM_H
